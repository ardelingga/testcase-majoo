const statusOk = 200;
const statusBadRequest = 400;
const statusTokenInvalid = 401;
const statusNoAuthorized = 403;
const statusNotFound = 404;
const statusUnProcessableEntity = 422;
const statusUpgradeRequired = 426;
const statusInternalError = 500;
