import 'dart:io';
import 'package:majootestcase/business_logic/models/user.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart' as sqlite;

SqliteDatabase sql = SqliteDatabase();

class SqliteDatabase {
  sqlite.Database? db;
  static String dbName = "testcase_mojoo.db";

  Future<void> init() async {
    Directory directory = await getApplicationDocumentsDirectory();
    final pathDB = join(directory.path, dbName);

    db = await sqlite.openDatabase(
      pathDB,
      version: 1,
      onCreate: (sqlite.Database database, int version) async {
        db = database;
        await this.createTables(User.tableName);
      },
    );
  }

  Future<void> createTables(String tableName) async {
    await db!.execute(""" CREATE TABLE $tableName (
        id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        email String(50),
        username String(50),
        password String(255),
        createdAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
      )
      """);
  }
}
