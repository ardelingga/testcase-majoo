import 'dart:async';
import 'package:dio/dio.dart';
import 'package:majootestcase/business_logic/config/app_config.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

Dio? dioInstance;
createInstance() async {

  var options = BaseOptions(
      baseUrl: AppConfig.BASE_URL_ENDPOINT,
      connectTimeout: 12000,
      receiveTimeout: 12000,
      headers: {
        "x-rapidapi-key": "9a3feefe2cmsh52cdb4f20989bacp135a27jsn74d6033c9c6c",
        "x-rapidapi-host": "imdb8.p.rapidapi.com"
      });
  dioInstance = new Dio(options);
  dioInstance!.interceptors.add(PrettyDioLogger(
      requestHeader: true,
      requestBody: true,
      responseBody: true,
      responseHeader: false,
      error: true,
      compact: true,
      maxWidth: 90));

}

Future<Dio> dio() async {
  await createInstance();
  return dioInstance!;
}

