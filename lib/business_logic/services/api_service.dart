import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:majootestcase/business_logic/models/movie_response.dart';
import 'package:majootestcase/business_logic/models/news_model.dart';
import 'package:majootestcase/business_logic/utils/error_helper.dart';
import 'package:majootestcase/business_logic/utils/fluttertoast_notify.dart';
import 'package:majootestcase/business_logic/services/dio_config_service.dart'
    as dioConfig;

class ApiServices {
  Future<MovieResponse?> getMovieList(url, params) async {
    try {
      var dio = await dioConfig.dio();
      Response<String> response = await dio.get(url, queryParameters: params);
      MovieResponse movieResponse =
          MovieResponse.fromJson(jsonDecode(response.data!));
      return movieResponse;
    } on DioError catch (e) {
      String msgError = ErrorHelper.getErrorMessage(e);
      FluttertoastNotify.showWithoutContext(msgError,
          typeNotify: TypeNotify.failed);
    } catch (e) {
      print(e.toString());
      FluttertoastNotify.showWithoutContext("Something went wrong.",
          typeNotify: TypeNotify.failed);
      return null;
    }
  }

  Future<List<NewsModel>?> getNewsList(url, params) async {
    try {
      var dio = await dioConfig.dio();
      Response response = await dio.get(url, queryParameters: params);
      List listData = response.data['items'];

      return List<NewsModel>.generate(
        listData.length,
        (i) => NewsModel(
          head: listData[i]['head'],
          body: listData[i]['body'],
          img: listData[i]['image']['url'],
        ),
      );
    } on DioError catch (e) {
      String msgError = ErrorHelper.getErrorMessage(e);
      FluttertoastNotify.showWithoutContext(msgError,
          typeNotify: TypeNotify.failed);
      return null;
    } catch (e) {
      print(e.toString());
      FluttertoastNotify.showWithoutContext("Something went wrong.",
          typeNotify: TypeNotify.failed);
      return null;
    }
  }
}
