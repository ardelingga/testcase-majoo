import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:encrypt/encrypt.dart' as encript;
import 'package:flutter/material.dart';
import 'package:majootestcase/business_logic/config/app_config.dart';

class CommonUtils {
  static String encriptPassword(String? pass) {
    final key = encript.Key.fromUtf8(AppConfig.APP_KEY);
    final iv = encript.IV.fromLength(16);
    final encrypter =
        encript.Encrypter(encript.AES(key, mode: encript.AESMode.cbc));

    final passEncrypted = encrypter.encrypt(pass!, iv: iv).base64;

    return passEncrypted;
  }

  static bool validateEmail(String email) {
    final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
    if (email.isNotEmpty) {
      var matched = pattern.hasMatch(email);
      return matched;
    }
    return false;
  }

  static Future checkAConnectivityInternet() async {
    bool internet = false;
    try {
      var connectivityResult = await (Connectivity().checkConnectivity());

      if (connectivityResult == ConnectivityResult.mobile) {
        if (await CommonUtils.checkInternet()) {
          internet = true;
        }
      } else if (connectivityResult == ConnectivityResult.wifi) {
        if (await CommonUtils.checkInternet()) {
          internet = true;
        }
      } else {
        internet = false;
      }
    } catch (e) {
      debugPrint("ERROR => " + e.toString());
      internet = false;
    }

    return internet;
  }

  static Future checkInternet() async {
    bool? internet;
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        internet = true;
      }
    } on SocketException catch (_) {
      debugPrint("ERROR => " + _.toString());
      internet = false;
    }
    return internet;
  }
}
