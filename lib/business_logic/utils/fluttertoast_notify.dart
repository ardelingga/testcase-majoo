import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

enum TypeNotify { success, failed }

class FluttertoastNotify {
  static void show(BuildContext context, String msg,
      {TypeNotify typeNotify = TypeNotify.success}) {
    FToast? fToast = FToast();
    fToast.init(context);

    Widget toast = Container(
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25.0),
        color: typeNotify == TypeNotify.success ? Colors.green : Colors.red,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          typeNotify == TypeNotify.success
              ? const Icon(
                  Icons.check,
                  color: Colors.white,
                )
              : const Icon(
                  Icons.close,
                  color: Colors.white,
                ),
          const SizedBox(
            width: 10,
          ),
          Text(
            msg,
            style: const TextStyle(color: Colors.white, fontSize: 14),
          ),
        ],
      ),
    );

    fToast.showToast(
        child: toast,
        gravity: ToastGravity.BOTTOM,
        toastDuration: const Duration(seconds: 2));
  }

  static showWithoutContext(String msg, {TypeNotify typeNotify = TypeNotify.success}){
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: typeNotify == TypeNotify.success ? Colors.green : Colors.red,
        textColor: Colors.white,
        fontSize: 16.0
    );
  }
}