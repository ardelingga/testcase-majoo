import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

SharedpresfsUtils sharedpresfsUtils = SharedpresfsUtils();

class SharedpresfsUtils {
  SharedPreferences? sharedPreferences;

  Future<void> init() async {
    sharedPreferences = await SharedPreferences.getInstance();
  }

  Future saveString(String key, String value) async {
    await sharedPreferences!.setString(key, value);
  }

  Future<String?> readString(String key) async {
    return sharedPreferences!.getString(key);
  }

  Future saveBool(String key, bool value) async {
    await sharedPreferences!.setBool(key, value);
  }

  bool? readBool(String key) {
    return sharedPreferences!.getBool(key);
  }

  Future saveJsonString(String key, value) async {
    await sharedPreferences!.setString(key, json.encode(value));
  }

  Future readJsonString(String key) async {
    var data = sharedPreferences!.getString(key);

    return data != null ? json.decode(data) : data;
  }

  Future removeData(String key) async {
    await sharedPreferences!.remove(key);
  }
}
