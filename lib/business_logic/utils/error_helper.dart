import 'package:dio/dio.dart';
import 'package:majootestcase/business_logic/constants/response_status_constant.dart';

class ErrorHelper {
  static String getErrorMessage(error) {
    print('error helper === $error');
    String message = "Something went wrong.";
    if (error is DioError) {
      message = error.message;
    }
    return message;
  }

  static extractApiError(DioError e) {
    String msg = "Something went wrong";

    if (e.type == DioErrorType.response) {
      int statusCode = e.response!.statusCode!;

      if (statusCode == statusNotFound) {
        msg = "Not Found";
      } else if (statusCode == statusInternalError) {
        msg = "Internal Server Error";
      } else if (statusCode == statusBadRequest) {
        msg = e.response!.data['meta']['message'];
      } else if (statusCode == statusUnProcessableEntity) {
        msg = e.response!.data['meta']['message'];
      } else {
        msg = e.message.toString();
      }
    } else if (e.type == DioErrorType.connectTimeout) {
      msg = "Please check your internet connection and try again.";
    } else if (e.type == DioErrorType.cancel) {
      msg = "Cancel";
    }

    return msg;
  }
}
