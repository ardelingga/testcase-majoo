import 'package:majootestcase/business_logic/config/sqlite_database.dart';
import 'package:sqflite/sqflite.dart' as sqlite;

class User {
  static String tableName = "users";

  late final int? id;
  late final String? username;
  late final String email;
  late final String password;
  late final String? createdAt;

  User({
    this.id,
    this.username,
    required this.email,
    required this.password,
    this.createdAt,
  });

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    username = json['username'];
    email = json['email'];
    password = json['password'];
    createdAt = json['createdAt'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['username'] = username;
    _data['email'] = email;
    _data['password'] = password;
    _data['createdAt'] = createdAt;
    return _data;
  }

  static Future all() async {
    var users = await sql.db!.query(tableName, orderBy: "id");

    return List.generate(users.length, (i) {
      return User(
        id: users[i]['id'] as int,
        email: users[i]['email'] as String,
        username: users[i]['username'] as String,
        password: users[i]['password'] as String,
        createdAt: users[i]['createdAt'] as String,
      );
    });
  }

  static Future detail(int id) async {
    var users =
        await sql.db!.query(tableName, where: "id = ?", whereArgs: [id]);

    return List.generate(users.length, (i) {
      return User(
        id: users[i]['id'] as int,
        email: users[i]['email'] as String,
        username: users[i]['username'] as String,
        password: users[i]['password'] as String,
        createdAt: users[i]['createdAt'] as String,
      );
    });
  }

  static Future<int> create(
      String? username, String? email, String? password) async {
    var data = {
      'email': email,
      'username': username,
      'password': password,
    };

    int id = await sql.db!.insert(tableName, data,
        conflictAlgorithm: sqlite.ConflictAlgorithm.replace);
    return id;
  }

  static Future delete(int id) async {
    var result =
        await sql.db!.delete(tableName, where: "id = ?", whereArgs: [id]);

    return result;
  }

  static Future login(String email, String password) async {
    var users = await sql.db!.rawQuery(
        "SELECT * FROM $tableName WHERE email='$email' and password='$password'");
    return List.generate(users.length, (i) {
      return User(
        id: users[i]['id'] as int,
        email: users[i]['email'] as String,
        username: users[i]['username'] as String,
        password: users[i]['password'] as String,
        createdAt: users[i]['createdAt'] as String,
      );
    });
  }
}
