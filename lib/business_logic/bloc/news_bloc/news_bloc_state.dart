part of 'news_bloc_cubit.dart';

abstract class NewsBlocState extends Equatable {
  const NewsBlocState();

  @override
  List<Object> get props => [];
}

class NewsBlocInitial extends NewsBlocState {}

class NewsBlocLoading extends NewsBlocState {}

class NewsBlocNoConnection extends NewsBlocState {}

// ignore: must_be_immutable
class NewsBlocFailed extends NewsBlocState {
  String? msg;
  NewsBlocFailed({this.msg});
}

// ignore: must_be_immutable
class NewsBlocLoaded extends NewsBlocState {
  List<NewsModel> listNews;
  NewsBlocLoaded({required this.listNews});
} 