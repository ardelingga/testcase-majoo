import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/business_logic/models/news_model.dart';
import 'package:majootestcase/business_logic/services/api_service.dart';
import 'package:majootestcase/business_logic/utils/common_utils.dart';

part 'news_bloc_state.dart';

class NewsBlocCubit extends Cubit<NewsBlocState> {
  NewsBlocCubit() : super(NewsBlocInitial());

  Future fetchNews() async {
    emit(NewsBlocLoading());
    print("CONNECTION => " + (await CommonUtils.checkAConnectivityInternet()).toString());

    // if (!(await CommonUtils.checkAConnectivityInternet())) {
    //   emit(NewsBlocNoConnection());
    //   return;
    // }

    ApiServices apiServices = ApiServices();
    var params = {
      'nconst': "nm0001667",
    };

    List<NewsModel>? listData =
        await apiServices.getNewsList("actors/get-all-news", params);
    emit(NewsBlocLoaded(listNews: listData!));
  }

}
