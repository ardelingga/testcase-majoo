import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/business_logic/models/user.dart';
import 'package:majootestcase/business_logic/utils/common_utils.dart';
import 'package:majootestcase/business_logic/utils/sharedprefs_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit() : super(AuthBlocInitialState());

  void fetchHistoryLogin() async {
    emit(AuthBlocInitialState());
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool? isLoggedIn = sharedPreferences.getBool("is_logged_in");
    if (isLoggedIn == null) {
      emit(AuthBlocLoginState());
    } else {
      if (isLoggedIn) {
        emit(AuthBlocLoggedInState());
      } else {
        emit(AuthBlocLoginState());
      }
    }
  }

  Future<bool> loginUser(User user) async {
    emit(AuthBlocLoadingState());
    await Future.delayed(const Duration(milliseconds: 500));
    var users = await User.login(user.email, user.password);
    print("PASS => " + user.password.toString());
    print("USERS => " + users.toString());

    if (users.length > 0) {
      User userLogin = users[0];
      sharedpresfsUtils.saveJsonString("user_value", userLogin.toJson());
      await sharedpresfsUtils.saveBool("is_logged_in", true);
      emit(AuthBlocLoggedInState());
      return true;
    } else {
      emit(AuthBlocLoginState());
      return false;
    }
  }

  Future registerUser(User user) async {
    emit(AuthBlocLoadingState());
    await Future.delayed(const Duration(milliseconds: 500));
    int idUser = await User.create(
        user.username, user.email, CommonUtils.encriptPassword(user.password));
    var users = await User.detail(idUser);

    if (users.length > 0) {
      User userLogin = users[0];

      sharedpresfsUtils.saveJsonString("user_value", userLogin.toJson());
      await sharedpresfsUtils.saveBool("is_logged_in", true);
      emit(AuthBlocLoggedInState());
    } else {
      emit(AuthBlocLoginState());
    }
  }
}
