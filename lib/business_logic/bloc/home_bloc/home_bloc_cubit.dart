import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/business_logic/models/movie_response.dart';
import 'package:majootestcase/business_logic/services/api_service.dart';
import 'package:majootestcase/business_logic/utils/common_utils.dart';

part 'home_bloc_state.dart';

class HomeBlocCubit extends Cubit<HomeBlocState> {
  HomeBlocCubit() : super(HomeBlocInitialState());

  void fetchingData() async {
    emit(HomeBlocLoadingState());
    print("CONNECTION => " +
        (await CommonUtils.checkAConnectivityInternet()).toString());

    if (!(await CommonUtils.checkAConnectivityInternet())) {
      emit(HomeBlocNoConnectionInternetState());
      return;
    }

    ApiServices apiServices = ApiServices();
    var params = {
      'q': "game of thr",
    };

    MovieResponse? movieResponse = await apiServices.getMovieList(
      "auto-complete",
      params,
    );
    if (movieResponse == null) {
      emit(HomeBlocErrorState("Error Unknown"));
    } else {
      emit(HomeBlocLoadedState(movieResponse.data!));
    }
  }
}
