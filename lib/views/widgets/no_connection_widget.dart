import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/business_logic/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/business_logic/bloc/news_bloc/news_bloc_cubit.dart';
import 'package:majootestcase/views/widgets/custom_button.dart';

class NoConnectionWidget extends StatelessWidget {
  const NoConnectionWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    HomeBlocCubit homeBlocCubit = HomeBlocCubit();
    Size size = MediaQuery.of(context).size;
    return BlocProvider(
      create: (context) => homeBlocCubit,
      child: Scaffold(
        body: SizedBox(
          width: size.width,
          height: size.height,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Icon(
                Icons.wifi_off,
                size: 100,
              ),
              Text(
                "No Connection Internet!",
                style: TextStyle(
                  fontSize: 18,
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              SizedBox(
                width: size.width / 2.5,
                child: CustomButton(
                  text: 'Refresh',
                  onPressed: () {
                    BlocProvider.of<HomeBlocCubit>(context).fetchingData();
                    BlocProvider.of<NewsBlocCubit>(context).fetchNews();
                  },
                  height: 100,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
