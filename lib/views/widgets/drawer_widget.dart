import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/business_logic/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/business_logic/models/user.dart';
import 'package:majootestcase/business_logic/utils/next_screen_utils.dart';
import 'package:majootestcase/business_logic/utils/sharedprefs_utils.dart';
import 'package:majootestcase/views/ui/auth/login_page.dart';

class DrawerWidget extends StatefulWidget {
  const DrawerWidget({Key? key}) : super(key: key);

  @override
  _DrawerWidgetState createState() => _DrawerWidgetState();
}

class _DrawerWidgetState extends State<DrawerWidget> {
  @override
  void initState() {
    super.initState();
  }

  User? user;
  Future<void> initData() async {
    var userJson = await sharedpresfsUtils.readJsonString("user_value");
    if(userJson != null)
      setState(() => user = User.fromJson(userJson));
  }

  @override
  Widget build(BuildContext context) {
    initData();
    return Drawer(
      child: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: 220,
              width: MediaQuery.of(context).size.width,
              color: Colors.blue,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Padding(
                    padding: EdgeInsets.only(
                        top: MediaQuery.of(context).padding.top),
                    child: SizedBox(
                      height: 90,
                      width: 90,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(200),
                        child: Image.network(
                          "https://truesun.in/wp-content/uploads/2021/08/62681-flat-icons-face-computer-design-avatar-icon.png",
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  Text(
                    user == null ? "" : user!.username!,
                    overflow: TextOverflow.clip,
                    style: const TextStyle(
                      fontSize: 20,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const SizedBox(
                    height: 2,
                  ),
                  Text(
                    user == null ? "" : user!.email,
                    overflow: TextOverflow.clip,
                    style: const TextStyle(
                      fontSize: 16,
                      color: Colors.white,
                    ),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                ],
              ),
            ),
            ListView(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              children: [
                itemMenuListTile(
                  iconData: Icons.movie_creation_outlined,
                  nameMenu: "Movies",
                  onTap: () {
                    BlocProvider.of<HomeBlocCubit>(context).fetchingData();
                    Navigator.pop(context);
                  },
                ),
                itemMenuListTile(
                  iconData: Icons.logout,
                  nameMenu: "Logout",
                  onTap: () => handleLogout(context),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget itemMenuListTile(
      {required IconData iconData,
      required String nameMenu,
      required Function onTap}) {
    return ListTile(
      leading: Icon(
        iconData,
        color: Colors.black87,
      ),
      title: Align(
        alignment: const Alignment(-1.2, 0),
        child: Text(
          nameMenu,
          style: const TextStyle(
            color: Colors.black87,
            fontSize: 16,
          ),
        ),
      ),
      onTap: () => onTap(),
    );
  }

  Future handleLogout(BuildContext context) async {
    await sharedpresfsUtils.removeData("is_logged_in");
    await sharedpresfsUtils.removeData("user_value");
    nextScreenCloseOther(context, LoginPage());
  }
}
