import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/business_logic/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/business_logic/models/user.dart';
import 'package:majootestcase/business_logic/utils/common_utils.dart';
import 'package:majootestcase/business_logic/utils/fluttertoast_notify.dart';
import 'package:majootestcase/business_logic/utils/next_screen_utils.dart';
import 'package:majootestcase/views/ui/auth/register_page.dart';
import 'package:majootestcase/views/ui/home/home_page.dart';
import 'package:majootestcase/views/widgets/custom_button.dart';
import 'package:majootestcase/views/widgets/loading.dart';
import 'package:majootestcase/views/widgets/text_form_field.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginPage> {
  AuthBlocCubit authBlocCubit = AuthBlocCubit();
  final _emailController = TextController();
  final _passwordController = TextController();
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  bool _isObscurePassword = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: BlocProvider(
        create: (context) => authBlocCubit,
        child: BlocListener<AuthBlocCubit, AuthBlocState>(
          listener: (context, state) {
            if (state is AuthBlocLoggedInState) {
              nextScreenCloseOther(context, HomePage());
            }
          },
          child: BlocBuilder<AuthBlocCubit, AuthBlocState>(
            builder: (context, state) {
              return state is AuthBlocLoadingState
                  ? LoadingIndicator()
                  : SizedBox(
                      width: size.width,
                      height: size.height,
                      child: SingleChildScrollView(
                        child: Padding(
                          padding: EdgeInsets.only(
                              top: 75, left: 25, bottom: 25, right: 25),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                'Selamat Datang',
                                style: TextStyle(
                                  fontSize: 24,
                                  fontWeight: FontWeight.bold,
                                  // color: colorBlue,
                                ),
                              ),
                              Text(
                                'Silahkan login terlebih dahulu',
                                style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                              SizedBox(
                                height: 9,
                              ),
                              _form(),
                              SizedBox(
                                height: 50,
                              ),
                              CustomButton(
                                text: 'Login',
                                onPressed: handleLogin,
                                height: 100,
                              ),
                              // const SizedBox(
                              //   height: 20,
                              // ),
                              // CustomButton(
                              //   text: 'Print Users',
                              //   onPressed: () async {
                              //     List<User> users = await User.all();
                              //     print(users[0].email);
                              //     print(users[0].username);
                              //     print(users[0].password);
                              //   },
                              //   height: 100,
                              // ),
                              // const SizedBox(
                              //   height: 20,
                              // ),
                              // CustomButton(
                              //   text: 'Insert Usert',
                              //   onPressed: () async {
                              //     User.create(
                              //         "ardelingga@gmail.com",
                              //         "ardelingga",
                              //         CommonUtils.encriptPassword("123456"));
                              //   },
                              //   height: 100,
                              // ),
                              // const SizedBox(
                              //   height: 20,
                              // ),
                              // CustomButton(
                              //   text: 'Delete Usert',
                              //   onPressed: () {
                              //     User.delete(4);
                              //     debugPrint("DELETED USER");
                              //   },
                              //   height: 100,
                              // ),
                              // const SizedBox(
                              //   height: 20,
                              // ),
                              // CustomButton(
                              //   text: 'Login Usert',
                              //   onPressed: () async {
                              //     User.login(
                              //         "ardelingga@gmail.com",
                              //         CommonUtils.encriptPassword(
                              //             "ardelingga"));
                              //   },
                              //   height: 100,
                              // ),
                              // const SizedBox(
                              //   height: 20,
                              // ),
                              // CustomButton(
                              //   text: 'ENCRIPT PASSWORD',
                              //   onPressed: () {
                              //     CommonUtils.encriptPassword("ardelingga");
                              //   },
                              //   height: 100,
                              // ),
                              SizedBox(
                                height: 50,
                              ),
                              _register(),
                            ],
                          ),
                        ),
                      ),
                    );
            },
          ),
        ),
      ),
    );
  }

  Widget _form() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          CustomTextFormField(
            context: context,
            controller: _emailController,
            isEmail: true,
            hint: 'Example@123.com',
            label: 'Email',
            validator: (val) {
              final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
              if (val != null)
                return pattern.hasMatch(val) ? null : 'email is invalid';
            },
          ),
          CustomTextFormField(
            context: context,
            label: 'Password',
            hint: 'password',
            controller: _passwordController,
            isObscureText: _isObscurePassword,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _register() {
    return Align(
      alignment: Alignment.center,
      child: TextButton(
        onPressed: () async {
          nextScreen(context, RegisterPage());
        },
        child: RichText(
          text: TextSpan(
              text: 'Belum punya akun? ',
              style: TextStyle(color: Colors.black45),
              children: [
                TextSpan(
                  text: 'Daftar',
                ),
              ]),
        ),
      ),
    );
  }

  void handleLogin() async {
    final _email = _emailController.value;
    final _password = _passwordController.value;

    if (_email.isEmpty || _password.isEmpty) {
      FluttertoastNotify.show(context, "Form tidak boleh kosong!",
          typeNotify: TypeNotify.failed);
      return;
    }

    if (!CommonUtils.validateEmail(_email)) {
      FluttertoastNotify.show(context, "Masukan e-mail yang valid",
          typeNotify: TypeNotify.failed);
    }

    if (formKey.currentState?.validate() == true &&
        _email.isNotEmpty &&
        _password.isNotEmpty) {
      User user = User(
        email: _email,
        password: CommonUtils.encriptPassword(_password),
      );
      bool isValidLogin = await authBlocCubit.loginUser(user);
      debugPrint("LOGIN  => " + isValidLogin.toString());
      if (isValidLogin) {
        FluttertoastNotify.show(context, "Login Berhasil!",
            typeNotify: TypeNotify.success);
      } else {
        FluttertoastNotify.show(context, "Login Gagal!",
            typeNotify: TypeNotify.failed);
      }
    }
  }
}
