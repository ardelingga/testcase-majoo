import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/business_logic/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/business_logic/models/user.dart';
import 'package:majootestcase/business_logic/utils/common_utils.dart';
import 'package:majootestcase/business_logic/utils/fluttertoast_notify.dart';
import 'package:majootestcase/business_logic/utils/next_screen_utils.dart';
import 'package:majootestcase/views/ui/home/home_page.dart';
import 'package:majootestcase/views/widgets/custom_button.dart';
import 'package:majootestcase/views/widgets/loading.dart';
import 'package:majootestcase/views/widgets/text_form_field.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  AuthBlocCubit authBlocCubit = AuthBlocCubit();
  final _emailController = TextController();
  final _usernameController = TextController();
  final _passwordController = TextController();
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  bool _isObscurePassword = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: BlocProvider<AuthBlocCubit>(
        create: (context) => authBlocCubit,
        child: BlocListener<AuthBlocCubit, AuthBlocState>(
          listener: (context, state) {
            if (state is AuthBlocLoggedInState) {
              nextScreenCloseOther(context, HomePage());
            }
          },
          child: BlocBuilder<AuthBlocCubit, AuthBlocState>(
            builder: (context, state) {
              return state is AuthBlocLoadingState
                  ? LoadingIndicator()
                  : SizedBox(
                      width: size.width,
                      height: size.height,
                      child: Stack(
                        children: [
                          SingleChildScrollView(
                            child: Padding(
                              padding: EdgeInsets.only(
                                  top: 75, left: 25, bottom: 25, right: 25),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    'Register',
                                    style: TextStyle(
                                      fontSize: 24,
                                      fontWeight: FontWeight.bold,
                                      // color: colorBlue,
                                    ),
                                  ),
                                  Text(
                                    'Silahkan daftar terlebih dahulu',
                                    style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 9,
                                  ),
                                  _form(),
                                  SizedBox(
                                    height: 50,
                                  ),
                                  CustomButton(
                                    text: 'Register',
                                    onPressed: handleRegister,
                                    height: 100,
                                  ),
                                  SizedBox(
                                    height: 50,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    );
            },
          ),
        ),
      ),
    );
  }

  Widget _form() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          CustomTextFormField(
            context: context,
            controller: _usernameController,
            isEmail: true,
            hint: 'username',
            label: 'Username',
          ),
          CustomTextFormField(
            context: context,
            controller: _emailController,
            isEmail: true,
            hint: 'Example@123.com',
            label: 'Email',
            validator: (val) {
              final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
              if (val != null)
                return pattern.hasMatch(val) ? null : 'email is invalid';
            },
          ),
          CustomTextFormField(
            context: context,
            label: 'Password',
            hint: 'password',
            controller: _passwordController,
            isObscureText: _isObscurePassword,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  void handleRegister() async {
    final _username = _usernameController.value;
    final _email = _emailController.value;
    final _password = _passwordController.value;

    if (_username.isEmpty || _email.isEmpty || _password.isEmpty) {
      FluttertoastNotify.show(context, "Form tidak boleh kosong!",
          typeNotify: TypeNotify.failed);
      return;
    }

    if (!CommonUtils.validateEmail(_email)) {
      FluttertoastNotify.show(context, "Masukan e-mail yang valid",
          typeNotify: TypeNotify.failed);
    }

    if (formKey.currentState?.validate() == true &&
        _email.isNotEmpty &&
        _password.isNotEmpty) {
      User user = User(
        username: _username,
        email: _email,
        password: _password,
      );
      try {
        await authBlocCubit.registerUser(user);
        FluttertoastNotify.show(context, "Register Berhasil!");
      } catch (e) {
        FluttertoastNotify.show(context, e.toString(),
            typeNotify: TypeNotify.failed);
      }
    }
  }
}
