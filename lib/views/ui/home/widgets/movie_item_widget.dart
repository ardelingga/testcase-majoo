import 'package:flutter/material.dart';
import 'package:majootestcase/business_logic/models/movie_response.dart';
import 'package:majootestcase/business_logic/utils/next_screen_utils.dart';
import 'package:majootestcase/views/ui/home/detail_movie_page.dart';

// ignore: must_be_immutable
class MovieItemWidget extends StatelessWidget {
  MovieItemWidget({Key? key, required this.i, required this.data})
      : super(key: key);

  int i;
  Data data;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => nextScreen(
          context,
          DetailMoviePage(
            data: data,
            heroTag: "hero $i",
          )),
      child: Card(
        elevation: 10,
        // color: Colors.red,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0))),
        child: Column(
          // mainAxisAlignment: MainAxisAlignment.center,
          // crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Flexible(
              flex: 6,
              child: Hero(
                tag: "hero $i",
                child: Image.network(
                  data.i!.imageUrl!,
                  fit: BoxFit.fill,
                ),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Flexible(
              flex: 1,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: Text(
                  data.l!,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
                  textDirection: TextDirection.ltr,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
