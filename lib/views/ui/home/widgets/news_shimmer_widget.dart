import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class NewsShimmerWidget extends StatelessWidget {
  const NewsShimmerWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: Colors.grey[300]!,
      highlightColor: Colors.grey[100]!,
      enabled: true,
      child: Container(
        height: 300,
        color: Colors.white,
      ),
    );
  }
}
