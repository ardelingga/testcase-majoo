import 'package:card_swiper/card_swiper.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/business_logic/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/business_logic/bloc/news_bloc/news_bloc_cubit.dart';
import 'package:majootestcase/views/ui/home/widgets/movie_item_widget.dart';
import 'package:majootestcase/views/ui/home/widgets/movies_shimmer_widget.dart';
import 'package:majootestcase/views/ui/home/widgets/news_shimmer_widget.dart';
import 'package:majootestcase/views/widgets/drawer_widget.dart';
import 'package:majootestcase/views/widgets/error_screen.dart';
import 'package:majootestcase/views/widgets/no_connection_widget.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  NewsBlocCubit newsBlocCubit = NewsBlocCubit();
  HomeBlocCubit homeBlocCubit = HomeBlocCubit();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return MultiBlocProvider(
      providers: [
        BlocProvider<HomeBlocCubit>(
          create: (context) => homeBlocCubit..fetchingData(),
        ),
        BlocProvider<NewsBlocCubit>(
          create: (context) => newsBlocCubit..fetchNews(),
        ),
      ],
      child: BlocBuilder<HomeBlocCubit, HomeBlocState>(
        builder: (context, state) {
          if (state is HomeBlocNoConnectionInternetState) {
            return NoConnectionWidget();
          }
          return Scaffold(
            appBar: AppBar(
              title: Text("MOVIES"),
              centerTitle: true,
            ),
            drawer: DrawerWidget(),
            backgroundColor: Colors.white,
            body: SizedBox(
              width: size.width,
              height: size.height,
              child: Stack(
                children: [
                  SingleChildScrollView(
                    child: Column(
                      children: [
                        Container(
                          height: 240,
                          child: BlocBuilder<NewsBlocCubit, NewsBlocState>(
                              builder: (context, state) {
                            if (state is NewsBlocLoading) {
                              return NewsShimmerWidget();
                            } else if (state is NewsBlocLoaded) {
                              return Swiper(
                                itemCount: state.listNews.length > 10
                                    ? 10
                                    : state.listNews.length,
                                autoplay: true,
                                controller: SwiperController(),
                                pagination: const SwiperPagination(),
                                itemBuilder: (context, i) {
                                  return Container(
                                    height: 300,
                                    child: Stack(
                                      children: [
                                        SizedBox(
                                          width: double.infinity,
                                          height: 300,
                                          child: Image.network(
                                            state.listNews[i].img!,
                                            fit: BoxFit.fill,
                                          ),
                                        ),
                                        Align(
                                          alignment: Alignment.bottomLeft,
                                          child: Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Container(
                                              decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                                color: Colors.black
                                                    .withOpacity(0.5),
                                              ),
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(8.0),
                                                child: Text(
                                                  state.listNews[i].head!,
                                                  style: TextStyle(
                                                    fontSize: 24,
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.white,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  );
                                },
                              );
                            }
                            return Container();
                          }),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 16, vertical: 16),
                          child: BlocBuilder<HomeBlocCubit, HomeBlocState>(
                            builder: (context, state) {
                              if (state is HomeBlocLoadedState) {
                                return GridView.builder(
                                  itemCount: state.data.length,
                                  scrollDirection: Axis.vertical,
                                  physics: const NeverScrollableScrollPhysics(),
                                  reverse: false,
                                  shrinkWrap: true,
                                  gridDelegate:
                                      const SliverGridDelegateWithMaxCrossAxisExtent(
                                    maxCrossAxisExtent: 200,
                                    childAspectRatio: 3 / 4.5,
                                    crossAxisSpacing: 20,
                                    mainAxisSpacing: 20,
                                  ),
                                  itemBuilder: (context, index) {
                                    return MovieItemWidget(i: index, data: state.data[index],);
                                  },
                                );
                              } else if (state is HomeBlocLoadingState) {
                                return MoviesShimmerWidget();
                              } else if (state is HomeBlocInitialState) {
                                return Scaffold();
                              } else if (state is HomeBlocErrorState) {
                                return ErrorScreen(message: state.error);
                              }

                              return Center(
                                child: Text(kDebugMode
                                    ? "state not implemented //"
                                    : ""),
                              );
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
