import 'package:google_fonts/google_fonts.dart';
import 'package:majootestcase/business_logic/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/business_logic/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';
import 'package:majootestcase/business_logic/config/app_config.dart';
import 'package:majootestcase/business_logic/config/sqlite_database.dart';
import 'package:majootestcase/business_logic/utils/sharedprefs_utils.dart';
import 'package:majootestcase/views/ui/auth/login_page.dart';
import 'package:majootestcase/views/ui/home/home_page.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await sharedpresfsUtils.init();
  await sql.init();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: AppConfig.APP_NAME,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        textTheme: GoogleFonts.alegreyaSansTextTheme(
            Theme.of(context).textTheme,
          ),
      ),
      home: BlocProvider(
        create: (context) => AuthBlocCubit()..fetchHistoryLogin(),
        child: MyHomePageScreen(),
      ),
    );
  }
}

class MyHomePageScreen extends StatelessWidget {
  const MyHomePageScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBlocCubit, AuthBlocState>(builder: (context, state) {
      if (state is AuthBlocLoginState) {
        return LoginPage();
      } else if (state is AuthBlocLoggedInState) {
        return BlocProvider(
          create: (context) => HomeBlocCubit()..fetchingData(),
          child: HomePage(),
        );
      }

      return Scaffold(
        body: Center(
            child: Text(kDebugMode ? "state not implemented $state" : "")),
      );
    });
  }
}
